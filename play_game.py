

from collections import Counter
from functools import lru_cache
from random import choices, choice
from itertools import chain

from learner import Learner


# specific to the "game"

# possible moves
OPTIONS = ["R", "S", "P"]

# scoring of the moves
@lru_cache(maxsize=9) # 3x3 possible outcomes
def scorer(outcome:str) -> bool:
    if outcome in {"RS", "SP", "PR"}:
        return True
    return False

# possible strategies
# should account for t=0 i.e. history is empty list

ideal_response = {'P': 'S', 'R': 'P', 'S': 'R'}

def random_reply(p1hist, p2hist):
    return choices(OPTIONS, k=1)[0]

def single_event_proportional(p1hist, p2hist):
    """ When opponent plays R two-thirds of the time,
        respond with P two-thirds of the time.'
    """
    if (not p1hist) or (not p2hist):
        return random_reply(p1hist, p2hist)
    prediction = select_proportional(p2hist, OPTIONS)
    return ideal_response[prediction]


# Strategy Utilities #############################

def select_proportional(events, baseline=()):
    rel_freq = Counter(chain(baseline, events))
    population, weights = zip(*rel_freq.items())
    return choices(population, weights)[0]










# subclass the learner

class RSP(Learner):

    def strategy_wins(self, my_move, opponent_move) -> bool:
        outcome = "".join([my_move, opponent_move])
        return scorer(outcome)



def main():

    strats = [
              random_reply,
              single_event_proportional,
              ]

    lrn = RSP(strategies=strats, verbose=1)

    trials = 10
    for i in range(trials):
        print(f"turn {i+1}/{trials}")
        human_move = choice(OPTIONS)
        lrn.learn(opponent_moves=[human_move])
    lrn.show_success()
    print("\n\n")
    # if all the moves are known in advance, this is equivalent:

    lrn2 = RSP(strategies=strats, verbose=0)

    all_opponent_moves = choices(population=OPTIONS, k=10)

    lrn2.learn(all_opponent_moves)
    print()
    lrn2.show_history()

    return


if __name__ == "__main__":
    from os import system
    _ = system("cls")
    main()
